
provider "aws" {
  region = "ap-south-1"
}

resource "aws_instance" "instance" {
  key_name             = "tf-demo"
  ami                  = "ami-0b5bff6d9495eff69"
  instance_type        = "t2.micro"
  user_data            = file("bootstrap_script.sh")
  iam_instance_profile = "admin-test"
}